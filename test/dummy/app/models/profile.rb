class Profile < ApplicationRecord
  belongs_to :avatar, class_name: FlexaDownloadable::Upload, optional: true
end

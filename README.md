# FlexaDownloadable
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'flexa_downloadable'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install flexa_downloadable
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

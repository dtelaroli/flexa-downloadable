module FlexaDownloadable
  module DownloadHelper
    def link_to_file(file)
      link_to file.filename, file_path(file)
    end
    
    def image(file, **args)
      image_tag file_path(file, width = args[:w], height = args[:h]), args
    end
    
    def file_path(file, width = nil, height = nil)
      flexa_downloadable.image_path(id: file.id, filename: file.filename, width: width, height: height)
    end
  end
end
require_dependency "flexa_downloadable/application_controller"

module FlexaDownloadable
  class FlexaDownloadable::DownloadsController < ApplicationController
    def download
      id = params[:id].to_i
      klass = params[:klass].classify
      field = params[:field].downcase

      object = eval("::#{klass}.find(#{id})")
      file = object.send(field)
      exec(file)
    end
    
    def by_id
      file = FlexaDownloadable::Upload.find_by(file_params)
      exec(file)
    end
    
    def image
      file = FlexaDownloadable::Upload.find_by(file_params)
      exec(file, true)
    end
    
    private
      def exec(file, image = false)
        if file
          content = file.content
          if(image && needs_resize?)
            content = resize(content)
          end
          send_data(content, filename: file.filename, content_type: file.content_type, disposition: image ? 'inline' : 'attachment')
        else
          gflash :notice => 'Downloaded!'
          redirect_to :back
        end
      end
      
      def file_params
        params.permit(:id, :filename)
      end
      
      def resize(content)
        image = MiniMagick::Image.read(content)
        resized = image.resize("#{params[:width]}!x#{params[:height]}")
        resized.to_blob
      end
      
      def needs_resize?
        params[:width].present? || params[:height].present?
      end
  end
end

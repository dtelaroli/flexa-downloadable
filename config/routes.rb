FlexaDownloadable::Engine.routes.draw do
  get 'download/:id/:klass/:field' => 'downloads#download', as: 'flexa_download'
  get 'download/:id/:filename' => 'downloads#by_id', as: 'by_id', constraints: { :filename => /[^\/]+/ }
  get 'image/:id/:filename' => 'downloads#image', as: 'image', constraints: { :filename => /[^\/]+/ }
end
